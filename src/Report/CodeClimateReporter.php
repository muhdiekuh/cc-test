<?php

namespace PhpCsFixer\Report;

/**
 * @author Hans-Christian Otto <c.otto@suora.com>
 * @internal
 */
class CodeClimateReporter implements ReporterInterface
{
    public function getFormat()
    {
        return 'codeclimate';
    }

    /**
     * Process changed files array. Returns generated report.
     *
     * @param ReportSummary $reportSummary
     *
     * @return string
     */
    public function generate(ReportSummary $reportSummary)
    {
        $report = [];
        foreach($reportSummary->getChanged() as $fileName => $change) {
            foreach($change['appliedFixers'] as $fixerName) {

                $report[] = [
                    'description' => $fixerName,
                    'location.path' => $fileName,
                    'location.lines.begin' => 0,
                    'fingerprint' => md5($fileName . $fixerName),
                ];
            }
        }
        return json_encode($report);
    }
}
